import Yml from "yml"

const configs = Yml.load("./conf/application.yml")
const trendingTopic = configs.trendingTopic

const checkOccurences = (data, answer) => {
  var count = {}

  for (var i = 0; i < data.length; i++) {
    var num = data[i]
    count[num] = count[num] ? count[num] + 1 : 1
  }
  for (var r = 0; r < Object.values(count).length; r++) {
    if (
      Object.values(count)[r] >= trendingTopic &&
      !answer.find((el) => el === Object.keys(count)[r])
    ) {
      answer.push(Object.keys(count)[r])
    }
  }
  return answer
}

export default checkOccurences
