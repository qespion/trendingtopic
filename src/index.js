import Yml from "yml"
import fs from "fs"
import readline from "readline"
import checkOccurences from "./checkOccurences"

const configs = Yml.load("./conf/application.yml")
const inputFile = configs.data.input
const outputFile = configs.data.output
const hourlyMsg = configs.hourlyMsg

const rd = readline.createInterface({
  input: fs.createReadStream(inputFile),
  output: fs.createWriteStream(outputFile),
})

const run = async () => {
  let answer = []
  let lineBuffer = []

  for await (const line of rd) {
    lineBuffer.push(line)
    if (lineBuffer.length >= hourlyMsg) {
      answer = checkOccurences(lineBuffer, answer)
      lineBuffer.shift()
    }
  }
  answer.map((line) => {
    rd.output.write(line + "\n")
  })
  if (!answer.length) {
    rd.output.write("Pas de trending topic")
  }
  console.log("file written")
}

run()
