## RENDU

<ul>
<li>1. Using stream to read file input, this way you have the same behavior for a 10Mo or 1Go file</li>
<li>2. Config file in conf folder, you may change input/output paths. As well as the numbers of messages in an hour and the numbers of hashtags to be considered as a trending topic</li>
<li>3. Unit testing using jest</li>
</ul>
